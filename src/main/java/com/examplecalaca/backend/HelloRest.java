package com.examplecalaca.backend;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloRest {
	
	@GetMapping
	public String get() {
		return "Ok, deu certo [get]";
	}
	
	@PostMapping
	public String post() {
		return "Ok, deu certo [post]";
	}
	
	@DeleteMapping
	public String delete() {
		return "Ok, deu certo [delete]";
	}

}
